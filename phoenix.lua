local phoenix = {
    button = Menu.AddKeyOption({"Hero Specific", "Phoenix"}, "1. Phoenix combo key", Enum.ButtonCode.KEY_SPACE),
    button2 = Menu.AddKeyOption({"Hero Specific", "Phoenix"}, "2. Auto launch fire spirit key..If enemy is attacking", Enum.ButtonCode.KEY_LCONTROL),
    moving = false,
    delay = 0,
    delay2 = 0,
    lastpos = nil
}

function phoenix.OnUpdate()
    local me = Heroes.GetLocal()
    if me and NPC.GetUnitName(me) ~= "npc_dota_hero_phoenix" then return end
    local near_mouse, allied, enemy, enemies = Input.GetNearestHeroToCursor(Entity.GetTeamNum(me), Enum.TeamType.TEAM_ENEMY), nil, nil, {}
    if not target and near_mouse and NPC.IsPositionInRange(near_mouse, Input.GetWorldCursorPos(), 250) then
        target = near_mouse
    elseif target and (not Entity.IsAlive(me) or not Entity.IsAlive(target) or Entity.IsDormant(target)) then
        target = nil
    end
    if NPC.GetAbility(me, "special_bonus_unique_phoenix_4") and Ability.GetLevel(NPC.GetAbility(me, "special_bonus_unique_phoenix_4")) > 0 then
        range = 2800
        driving_time = 2.2
    else
        range = 1400
        driving_time = 0.9
    end
    for i, v in ipairs(Entity.GetHeroesInRadius(me, 1400 ,Enum.TeamType.TEAM_BOTH)) do
        if v and Entity.IsAlive(v) then
            if Entity.IsSameTeam(me, v) then
                allied = v
            else
                table.insert(enemies, v)
                if #enemies > 4 then
                    table.sort(enemies, function (a, b) return Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(a)):Length2D() > Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(b)):Length2D() end)
                end
                enemy = v
            end
        end
    end
    for i = 0, 5 do
        local ability = NPC.GetAbilityByIndex(me, i)
        if ability then
            if target then
                if Menu.IsKeyDown(phoenix.button) then
                    if NPC.HasModifier(me, "modifier_phoenix_sun_ray") and GameRules.GetGameTime() > phoenix.delay then
                        Player.PrepareUnitOrders(Players.GetLocal(), 1, nil, Entity.GetAbsOrigin(target), nil, Enum.PlayerOrderIssuer.DOTA_ORDER_ISSUER_HERO_ONLY, me)
                        phoenix.delay = GameRules.GetGameTime() + NetChannel.GetAvgLatency(Enum.Flow.FLOW_OUTGOING) + 0.3
                    end
                    if Ability.GetName(ability) == "phoenix_icarus_dive" and Ability.IsReady(ability) then
                        if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < range and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() > 600 and not NPC.HasModifier(me, "modifier_phoenix_sun_ray") and not NPC.HasState(target, Enum.ModifierState.MODIFIER_STATE_MAGIC_IMMUNE) and Entity.GetHealth(me) / Entity.GetMaxHealth(me) * 80 > Entity.GetHealth(target) / Entity.GetMaxHealth(target) * 100 then
                            Ability.CastPosition(ability, Entity.GetAbsOrigin(target))
                            phoenix.delay = GameRules.GetGameTime() + NetChannel.GetAvgLatency(Enum.Flow.FLOW_OUTGOING) + driving_time
                        end
                    elseif Ability.GetName(ability) == "phoenix_icarus_dive_stop" and Ability.IsReady(ability) then
                        if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() > 700 and GameRules.GetGameTime() > phoenix.delay then
                            Ability.CastNoTarget(ability)
                        end
                    elseif Ability.GetName(ability) == "phoenix_sun_ray" and Ability.IsReady(ability) then
                        if NPC.FindFacingNPC(me, nil, Enum.TeamType.TEAM_ENEMY, 1400, 120) == target then
                            Ability.CastPosition(ability, Entity.GetAbsOrigin(target))
                        end
                    elseif Ability.GetName(ability) == "phoenix_sun_ray" and Ability.GetCooldown(ability) > 0 then
                        phoenix.moving = false
                    elseif Ability.GetName(ability) == "phoenix_sun_ray_toggle_move" and Ability.IsReady(ability) then
                        if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < 2500 and NPC.HasModifier(me, "modifier_phoenix_sun_ray") then
                            if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() > 900 and not phoenix.moving then
                                Ability.CastNoTarget(ability)
                                phoenix.moving = true
                            elseif Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < 900 and phoenix.moving then
                                Ability.CastNoTarget(ability)
                                phoenix.moving = false
                            end
                        end
                    end
                else
                    target = nil
                end
            end
            if enemy then
                local autonova, dmg = true, 0
                if Menu.IsKeyDown(phoenix.button) or Menu.IsKeyDown(phoenix.button2) then
                    if #Heroes.InRadius(Entity.GetAbsOrigin(me), NPC.GetAttackRange(enemy) + 250, Entity.GetTeamNum(me), Enum.TeamType.TEAM_ENEMY) > 1 then
                        autonova = false
                    end
                    if Ability.GetName(ability) == "phoenix_fire_spirits" and Ability.IsReady(ability) then
                        if enemy and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemy)):Length2D() < 1400 then
                            Ability.CastNoTarget(ability)
                        end
                    elseif Ability.GetName(ability) == "phoenix_launch_fire_spirit" and Ability.IsReady(ability) then
                        if enemies[1] and not NPC.HasModifier(enemies[1], "modifier_phoenix_fire_spirit_burn") and not NPC.HasState(enemies[1], Enum.ModifierState.MODIFIER_STATE_MAGIC_IMMUNE) then
                            if not NPC.IsRunning(enemies[1]) or Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemies[1])):Length2D() < NPC.GetAttackRange(enemies[1]) + 50 then
                                pos = Entity.GetAbsOrigin(enemies[1])
                            else
                                pos = Entity.GetAbsOrigin(enemies[1]) + Entity.GetRotation(enemies[1]):GetForward():Normalized():Scaled(NPC.GetBaseSpeed(enemies[1]) + NPC.GetMoveSpeed(enemies[1]) - NPC.GetBaseSpeed(enemies[1]))
                            end
                            if not phoenix.lastpos or phoenix.lastpos and pos:Distance(phoenix.lastpos):Length2D() > 500 then
                                Ability.CastPosition(ability, pos)
                                phoenix.lastpos = pos
                                phoenix.delay2 = GameRules.GetGameTime() + NetChannel.GetAvgLatency(Enum.Flow.FLOW_OUTGOING) + 3
                            end
                        end
                    elseif GameRules.GetGameTime() > phoenix.delay2 and phoenix.lastpos ~= nil then
                        phoenix.lastpos = nil
                    elseif Ability.GetName(ability) == "phoenix_supernova" and Ability.IsReady(ability) then
                        if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemy)):Length2D() < 1400 and autonova and not Menu.IsKeyDown(phoenix.button2) then
                            if NPC.GetItem(me, "item_ultimate_scepter") or NPC.HasModifier(me, "modifier_item_ultimate_scepter_consumed") then
                                if allied and Entity.GetHealth(allied) / Entity.GetMaxHealth(allied) < 0.15 and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(allied)):Length2D() < 500 then
                                    Ability.CastTarget(ability, allied)
                                elseif Entity.GetHealth(me) / Entity.GetMaxHealth(me) < 0.15 then
                                    Ability.CastTarget(ability, me)
                                end
                            else
                                if Entity.GetHealth(me) / Entity.GetMaxHealth(me) < 0.15 then
                                    Ability.CastNoTarget(ability)
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

function phoenix.OnGameEnd()
    phoenix.delay = 0
    phoenix.delay2 = 0
    phoenix.lastpos = nil
end

return phoenix